﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using digitalgreencard.Views;

namespace digitalgreencard
{
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();

            // set the initial page
            CurrentItem.CurrentItem = CurrentItem.Items[1];
        }
    }
}
